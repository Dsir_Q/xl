package com.dqsir.xl.xl_demo.annotation;

import java.lang.annotation.*;

@Target({ElementType.METHOD ,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RefuseAnnotation {

    //延时
    long timout();

    // 速度  每speed毫秒数后产生一个令牌
    long speed();
}

