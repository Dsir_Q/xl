package com.dqsir.xl.xl_demo.aspect;

import com.dqsir.xl.xl_demo.annotation.RefuseAnnotation;
import com.dqsir.xl.xl_demo.xl.MyRefuserResult;
import com.dqsir.xl.xl_demo.xl.DefualtRefuse;
import com.dqsir.xl.xl_demo.xl.I.IRefuse;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * aop限流拦截
 */
@Aspect
@Configuration
public class RefuseAspect {
    private static final Logger logger = LoggerFactory.getLogger(RefuseAspect.class);
    ConcurrentHashMap<String, IRefuse> map = new ConcurrentHashMap<>();

    @Pointcut("@annotation(com.dqsir.xl.xl_demo.annotation.RefuseAnnotation)")
    public void RefusePointCut() {}

    private  AtomicInteger a = new AtomicInteger();
    @Around("RefusePointCut()")
    public Object around(ProceedingJoinPoint joinPoint) {
        RefuseAnnotation refuseAnnotation = getRefuseAnnotation(joinPoint);
        if(refuseAnnotation == null ){
            throw new RuntimeException("不存在限流注解");
        }else{
            //方法名字
            String name = getRefuseMethod(joinPoint).getName();
            //根据方法名字去map中获取限流方法
            IRefuse refuse = map.get(name);
            if(refuse == null ){
                refuse =  new DefualtRefuse().init(new MyRefuserResult());
                map.put(name,refuse);
            }
            if(!refuse.isopen()){
                // 拦截返回
                return refuse.r();
            }else{
                try {
                    logger.info("放行");
                    // 放行
                    return joinPoint.proceed();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        }
        return null;
    }

    //获取注解
    private RefuseAnnotation  getRefuseAnnotation(ProceedingJoinPoint joinPoint){
        // 获取注解参数
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        return method.getAnnotation(RefuseAnnotation.class);
    }


    //获取方法
    private Method  getRefuseMethod(ProceedingJoinPoint joinPoint){
        // 获取注解参数
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        return method;
    }


}
