package com.dqsir.xl.xl_demo.controller;

import com.dqsir.xl.xl_demo.annotation.RefuseAnnotation;
import com.dqsir.xl.xl_demo.util.R;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController
public class index {

    @GetMapping("/")
    @RefuseAnnotation(speed = 1000 , timout = 50)
    public R  index(){
        return R.ok("success");
    }

    @GetMapping("/b")
    @RefuseAnnotation(speed = 100 , timout = 50)
    public R  index2(){
        return R.ok("success");
    }

}
