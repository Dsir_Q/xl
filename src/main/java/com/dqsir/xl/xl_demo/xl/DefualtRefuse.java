package com.dqsir.xl.xl_demo.xl;

import com.dqsir.xl.xl_demo.annotation.RefuseAnnotation;
import com.dqsir.xl.xl_demo.util.R;
import com.dqsir.xl.xl_demo.xl.I.IRefuse;
import com.dqsir.xl.xl_demo.xl.I.IRefuseResult;
import com.dqsir.xl.xl_demo.xl.I.IRefuseStrategy;

public class DefualtRefuse implements IRefuse<R>   {


//    决绝结果
    private IRefuseResult<R>  refuseResult;
//    拒绝策略
    private IRefuseStrategy refuseStrategy;

    /**
     * 判断是否需要拦截
     * @return
     */
    @Override
    public boolean isopen(){
        return refuseStrategy.isOpen();
    }

    /**
     * 获取拒绝后的结果
     * @return
     */
    @Override
    public R r(){
        return  refuseResult.result();
    }


    public IRefuseResult getRefuseResult() {
        return refuseResult;
    }

    public void setRefuseResult(IRefuseResult refuseResult) {
        this.refuseResult = refuseResult;
    }

    public IRefuseStrategy getRefuseStrategy() {
        return refuseStrategy;
    }

    public void setRefuseStrategy(IRefuseStrategy refuseStrategy) {
        this.refuseStrategy = refuseStrategy;
    }


    public DefualtRefuse init(IRefuseResult<R> refuseResult ,IRefuseStrategy refuseStrategy) {
        DefualtRefuse refuse = new DefualtRefuse();
        refuse.setRefuseResult(refuseResult);
        refuse.setRefuseStrategy(refuseStrategy);
        return refuse;
    }

    public DefualtRefuse init(IRefuseResult<R> refuseResult,RefuseAnnotation refuseAnnotation) {
        DefualtRefuse refuse = new DefualtRefuse();
        refuse.setRefuseResult(refuseResult);
        refuse.setRefuseStrategy(new DefualtResultStrategy(refuseAnnotation));
        return refuse;
    }

    public DefualtRefuse init(IRefuseResult<R> refuseResult) {
        DefualtRefuse refuse = new DefualtRefuse();
        refuse.setRefuseResult(refuseResult);
        //设置默认 100毫秒产生一个令牌  延时等待获取 50毫秒
        refuse.setRefuseStrategy(new DefualtResultStrategy(100l,50l));
        return refuse;
    }

    public DefualtRefuse init(RefuseAnnotation refuseAnnotation) {
        DefualtRefuse refuse = new DefualtRefuse();
        refuse.setRefuseResult(new DefualtRefuseResult());
        refuse.setRefuseStrategy(new DefualtResultStrategy(refuseAnnotation));
        return refuse;
    }
}
