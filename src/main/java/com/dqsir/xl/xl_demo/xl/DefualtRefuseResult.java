package com.dqsir.xl.xl_demo.xl;

import com.dqsir.xl.xl_demo.util.R;
import com.dqsir.xl.xl_demo.xl.I.IRefuseResult;

public class DefualtRefuseResult  implements IRefuseResult {
    @Override
    public R result() {
        return R.ok("throttle");
    }
}
