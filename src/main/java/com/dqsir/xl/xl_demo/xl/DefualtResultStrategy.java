package com.dqsir.xl.xl_demo.xl;


import com.dqsir.xl.xl_demo.annotation.RefuseAnnotation;
import com.dqsir.xl.xl_demo.xl.I.IBucket;
import com.dqsir.xl.xl_demo.xl.I.IRefuseStrategy;

public class DefualtResultStrategy implements IRefuseStrategy {

    private IBucket tokenBucket = null;

    DefualtResultStrategy(RefuseAnnotation refuseAnnotation){
        tokenBucket = new TokenBucket(refuseAnnotation.speed(),refuseAnnotation.timout());
    }

    DefualtResultStrategy(Long s ,Long t){
        tokenBucket = new TokenBucket(s,t);
    }

    @Override
    public boolean isOpen() {
//        请求获取令牌，能够获取到令牌就放行，不能获取到令牌就拦截
        return tokenBucket.getToken();
    }

}

