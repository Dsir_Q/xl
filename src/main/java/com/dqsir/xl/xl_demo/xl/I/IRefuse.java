package com.dqsir.xl.xl_demo.xl.I;

import com.dqsir.xl.xl_demo.util.R;

public interface IRefuse<T> {
    //是否放流
    boolean isopen();
    //获取结果
    T r();
}
