package com.dqsir.xl.xl_demo.xl.I;

/**
 * 拒绝返回
 */
public interface IRefuseResult<T> {
    T result();
}
