package com.dqsir.xl.xl_demo.xl.I;

/**
 * 限流策略
 */
public interface IRefuseStrategy {
    boolean isOpen();
}
