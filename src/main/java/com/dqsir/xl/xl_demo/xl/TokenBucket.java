package com.dqsir.xl.xl_demo.xl;

import com.dqsir.xl.xl_demo.xl.I.IBucket;

import java.util.concurrent.atomic.AtomicLong;

//  令牌桶
class TokenBucket implements IBucket {
    private volatile AtomicLong atomicLong = new AtomicLong();
    private Long timeout;
    private Long sec;
    public TokenBucket(Long sec,Long timeout) {
        this.sec = sec;
        init();
        this.timeout = timeout;
    }

    private void init() {
        //计算桶的容量
        long l = 1000 / sec;
        Thread t = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(sec);
                    // 桶没有满就往里面加令牌
                    if (atomicLong.get() < l) {
                        System.out.println("令牌+1");
                        atomicLong.incrementAndGet();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.setDaemon(true);
        t.start();
    }

    @Override
    public boolean getToken() {
        return getToken(0);
    }

    public boolean getToken(int size) {
        if(size == 0){
            long l = atomicLong.get();
            if (l > 0) {
                return atomicLong.compareAndSet(l, l - 1);
            } else {
                getToken(1);
                return false;
            }
        }else{
            long l = atomicLong.get();
            if (l > 0) {
                return atomicLong.compareAndSet(l, l - 1);
            } else {
                return false;
            }
        }

    }
}
